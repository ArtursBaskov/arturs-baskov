
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".field").not("." + optionValue).hide();
                $("." + optionValue).show();
                $('.dynamicRequire').removeAttr('required'); 
                $("." + optionValue + " input").attr('required','true'); 
            } else{
                $(".field").hide();
            }
        });
    }).change();
});


