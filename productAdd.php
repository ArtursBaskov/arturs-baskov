<?php
require_once("config.php");
use Task\Classes\DbPDO;
use Task\Classes\Table; 
use Task\Classes\Create;
if (isset($_POST['save'])) {
    $createObj = new Create();
    $createObj->createProduct();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
     <title>Product</title>
</head> 
<body>
    
<header>
    <p class="headingText">Product Add</p> 
    <nav>
        <ul class="navBar">
         <li> <button form="myForm" type="submit" id="save" name="save" value="Save" method="POST" >Save</button> </li> 
        <li> <button class="cancel" > <a href="list.php"> Cancel </a> </button> </li>
        </ul>
    </nav>
</header>
<hr class="nav-bottom">
<div class="ProductCreate-container">
    <form id="myForm" name="ProductForm" class="Product-input"  method="POST" >
    
    <div>
        <label>SKU</label>
        <input  type="text" id="sku" name="sku" required  oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');">
    </div>
    <div>
        <label>Name</label>
        <input type="text" id="product_name" name="product_name" required oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');">
    </div>
    <div>
        <label>Price ($)</label>
        <input pattern="^[0-9]+(\.[0-9]{1,2})?$" id="price" name="price" required oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');" >
    </div>
    <div> 
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="javascript/typehandling.js"></script>
    <script type="text/javascript" src="javascript/clearInputFields.js"></script>
    <label>Type Swithcer</label>
        <select id="type-switcher" name="type-switcher" >
            <option value="sizeMB"  >DVD-disc</option>
            <option value="weightKg">Book</option>
            <option value="dimensions">Furniture</option>
        </select>
    <div class="sizeMB field">
        <label>Size (MB)</label>
        <input pattern="\d*" id="sizeMB" class="dynamicRequire" name="sizeMB"  onfocus="whenDVDSelected()" oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');">
        <p>Please provide size in MB</p>
    </div>
    <div class="weightKg field">
        <label>Weight (Kg)</label>
        <input pattern="\d*" id="weightKg" class="dynamicRequire" name="weightKg" onfocus="whenBookSelected()" oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');">
        <p>Please provide weight in Kg</p>
    </div>
    <div class="dimensions field">
        <label>Height (CM)</label>
        <input pattern="\d*" id="height" class="dynamicRequire" name="height" onfocus="whenDimensionsSelected()" oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');"> <br>
        <label>Width (CM)</label>
        <input pattern="\d*" id="width" class="dynamicRequire" name="width" onfocus="whenDimensionsSelected()" oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');"> <br>
        <label>Length (CM)</label>
        <input pattern="\d*" id="lengthCM" class="dynamicRequire" name="lengthCM" onfocus="whenDimensionsSelected()" oninput="setCustomValidity(''); checkValidity(); setCustomValidity(validity.valid ? '' :'Please, provide the data of indicated type');">
        <p>Please provide dimensions in HxWxL format</p>
    </div>
    
    </form>   
</div>    
<div class="bottomText">
<hr class="page bottom">
<p1>Scandiweb Test assignment</p1>
</div>
</body>
    
</html>