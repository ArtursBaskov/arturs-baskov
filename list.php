<?php
require_once("config.php");
use Task\Classes\DbPDO;
use Task\Classes\Table; 
use Task\Classes\Delete; 

if (isset($_POST['delete'])){
$deleteObj = new Delete();
$deleteObj->deleteProducts();
}    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <title>Product</title>
</head> 
<body>
<header>
<p class="headingText">Product List</p> 
<nav>
    <ul class="navBar">
        <li> <button class="add"> <a href="productAdd.php"> Add </a> </button> </li>
        <li> <button form="productForm" type="submit" id="delete" name="delete" value="Delete" method="POST" onclick="deleteProducts()">Mass Delete</button> </li>
    </ul>
</nav>
</header>
<hr class="nav-bottom">
<div class="Product-container">

<form id="productForm" method="POST">
    <div class="productRow" >
    <?php 
        $tableObj = new Table();
        $tableObj->getProductDiscs(); 
    ?>
    </div>

    <div class="productRow2" >
    <?php 
        $tableObj = new Table();
        $tableObj->getProductBooks();
    ?>
    </div>

    <div class="productRow3" >
    <?php 
        $tableObj = new Table();
        $tableObj->getProductFurniture();
    ?>
    </div>
    </form>
</div>
<div class="bottomText">
<hr class="page bottom">
<p1>Scandiweb Test assignment</p1>
</div>

</body>
    
</html>