<?php 
namespace Task\Classes;
use \PDO;


class DbPDO {
  private $host = "127.0.0.5:3306";
  private $user = "root";
  private $password = "PrakseSql3008";
  private $dbname = "product_list";

  protected function connect() {
    try {
      $dsn = 'mysql:host=' . $this->host .';dbname='. $this->dbname;
      $pdo = new PDO($dsn, $this->user, $this->password);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        die($e->getMessage());
      }
    return $pdo;   
  }
}
