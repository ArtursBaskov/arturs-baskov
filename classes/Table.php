<?php
namespace Task\Classes;

class Table extends DbPDO {
  
  
  public function getProductDiscs() {
    $sql = "SELECT * FROM products WHERE ( sizeMB != 0 )";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()){
      $id = $row['idproducts'];
      echo '<ul>';
      echo '<input type="checkbox" id="deleteBox[]" name="deleteBox[]" value="' . $id .'">';
      echo '<li>';
      echo $row['sku'] . '<br>';
      echo $row['product_name'] . '<br>';
      echo $row['price'] ;
      echo " $" . '<br>';
      echo "Size: " . $row['sizeMB'] . " MB";
      echo '</li>';
      echo '</ul>';
      
    } 
  }  

  public function getProductBooks() {
    $sql = "SELECT * FROM products WHERE ( weightKg != 0 ) ";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()){
      $id = $row['idproducts'];
      echo '<ul>';
      echo '<input type="checkbox" id="deleteBox[]" name="deleteBox[]" value="' . $id .'">';
      echo '<li>';
      echo $row['sku'] . '<br>';
      echo $row['product_name'] . '<br>';
      echo $row['price'] ;
      echo " $" . '<br>';
      echo "Weight: " . $row['weightKg'] . " Kg";
      echo '</li>';
      echo '</ul>';
    } 
  }
  
  public function getProductFurniture() {
    $sql = "SELECT * FROM products WHERE ( height != 0 AND width != 0 AND lengthCM != 0 ) ";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()){
      $id = $row['idproducts'];
      echo '<ul>';
      echo '<input type="checkbox" id="deleteBox[]" name="deleteBox[]" value="' . $id .'">';
      echo '<li>';
      echo $row['sku'] . '<br>';
      echo $row['product_name'] . '<br>';
      echo $row['price'] . " $" . '<br>' ;
      echo "Dimension: " . $row['height'] . "x" . $row['width'] . "x" . $row['lengthCM'];
      echo '</li>';
      echo '</ul>';
    } 
  }  
}
 