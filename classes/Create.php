<?php
namespace Task\Classes;
class Create extends DbPDO {
  public $sku;
  public $product_name;
  public $price;
  public $sizeMB;
  public $weightKg;
  public $height;
  public $width;
  public $lengthCM;

  public function createProduct() {
    try{
      $sql = "INSERT INTO products(sku, product_name, price, sizeMB, weightKg, height, width, lengthCM) VALUES (:sku, :product_name, :price, :sizeMB, :weightKg, :height, :width, :lengthCM)";
      $stmt = $this->connect()->prepare($sql);
      
    
      $stmt->bindParam(":sku", $sku);
      $stmt->bindParam(":product_name", $product_name);
      $stmt->bindParam(":price", $price);
      $stmt->bindParam(":sizeMB", $sizeMB);
      $stmt->bindParam(":weightKg", $weightKg);
      $stmt->bindParam(":height", $height);
      $stmt->bindParam(":width", $width);
      $stmt->bindParam(":lengthCM", $lengthCM);



      $sku = $_POST["sku"];
      $product_name = $_POST["product_name"];
      $price = $_POST["price"];
      $sizeMB = $_POST["sizeMB"];
      $weightKg = $_POST["weightKg"];
      $height = $_POST["height"];
      $width = $_POST["width"];
      $lengthCM = $_POST["lengthCM"];
      $stmt->execute();
      header("Location:list.php");
    } catch (PDOException $e) {
        die($e->getMessage());
      } 
  }
}